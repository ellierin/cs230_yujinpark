#pragma once

#include <string>
#include "Texture.h"
#include "BasicDataTypes.h"

class Background {
public:

	Background() : cameraRange{ {0 ,0}, {0, 0} } {};
	void Add(const std::string& texturePath, int level);
	void Unload();
	void Draw(Vector2D camera);
	Rect GetCameraRange();


private:
	struct ParallaxInfo {
		Texture texture;
		int level;
	};
	Rect cameraRange;
	std::vector<ParallaxInfo> backgrounds;
};
