/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Texture.h
Purpose: Header file for Texture
Project: CS230
Author: Yujin Park
Creation date: 4/03/2020
-----------------------------------------------------------------*/
#pragma once

#include "doodle/image.hpp"
#include "BasicDataTypes.h"

class Texture {
public:
	Texture();
	void Load(const std::filesystem::path& filePath);
	Texture(const std::filesystem::path& filePath);
	void Draw(Vector2D location);
	Vector2DInt GetSize();
private:
	doodle::Image image;
	Vector2DInt size;
};
