/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Engine.cpp
Purpose: source file for Engine Module
Project: CS230
Author: Kevin Wright / Yujin Park
Creation date: 3/19/2020
-----------------------------------------------------------------*/
#include <chrono>

#include "Logger.h"
#include "GameStateManager.h"
#include "Engine.h"

Engine::Engine() : gameStateManager(),
#ifdef _DEBUG
							logger(Logger::Severity::Debug, true)
#else 
							logger(Logger::Severity::Event, false)
#endif
{}
Engine::~Engine() {
}

void Engine::Update() {
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();

	double dt = std::chrono::duration<double>(now - lastTick).count();

	if (dt >= 1 / Target_FPS) {


		GetGameStateManager().Update(dt);
		GetInput().Update();
		GetWindow().Update();
		lastTick = now;
		timer += dt;

		++frameCount;
		int time = 5;

		if (timer >= time) {

			aveFrameRate = frameCount / timer;
			GetLogger().LogEvent("FPS:  " + std::to_string(aveFrameRate));
			frameCount = 0;
			timer = 0;
		}
		GetLogger().LogVerbose("Engine Update");
	}
}

void Engine::Shutdown() {
	GetLogger().LogEvent("Engine Shutdown");
}

void Engine::Init(std::string windowName) {

	lastTick = std::chrono::system_clock::now();
	GetWindow().Init(windowName);
	GetLogger().LogEvent("Engine Init");
}

bool Engine::HasGameEnded(){
	return GetGameStateManager().HasGameEnded();
}


