/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Sprite.cpp
Purpose: Source file for Sprite
Project: CS230
Author: Yujin Park
Creation date: 4/02/2020
-----------------------------------------------------------------*/

#include "Sprite.h"
#include "../Game/Hero.h"

void Sprite::Load(std::string texturePath) {
	texture.Load(texturePath);
}

void Sprite::Draw(Vector2D position) {
	texture.Draw(Vector2D{position.x - hotSpot.x, position.y - hotSpot.y});
}

void Sprite::SetHotSpot(Vector2DInt position) {
	hotSpot = position;
}

Vector2DInt Sprite::GetSize() {
	return Vector2DInt(texture.GetSize());
}