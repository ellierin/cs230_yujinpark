/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Texture.cpp
Purpose: Source file for Texture
Project: CS230
Author: Yujin Park
Creation date: 4/03/2020
-----------------------------------------------------------------*/

#include "doodle/drawing.hpp"
#include "Texture.h"

Texture::Texture() { }

void Texture::Load(const std::filesystem::path& filePath) {
	image = doodle::Image{ filePath };
	size = { image.GetWidth(), image.GetHeight() };
}

void Texture::Draw(Vector2D location) {
	doodle::draw_image(image, location.x, location.y);
}

Vector2DInt Texture::GetSize() { return size; }

Texture::Texture(const std::filesystem::path& filePath) : image(filePath) {
	size = { image.GetWidth(), image.GetHeight() };
}