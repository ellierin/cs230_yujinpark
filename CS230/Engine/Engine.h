/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Engine.h
Purpose: Header file for Engine Module
Project: CS230
Author: Kevin Wright / Yujin Park
Creation date: 3/19/2020
-----------------------------------------------------------------*/

#pragma once

#include <chrono>
#include "BasicDataTypes.h"
#include "GameStateManager.h"
#include "Input.h"
#include "Window.h"
#include "Logger.h"


class Engine {
public:
	static Engine& Instance() { static Engine instance; return instance; }
	static Logger& GetLogger() { return Instance().logger; };
	static Input& GetInput() { return Instance().input; }
	static Window& GetWindow() { return Instance().window; }
	static GameStateManager& GetGameStateManager() { return Instance().gameStateManager; }

	void Init(std::string windowName);
	void Shutdown();
	void Update();
	bool HasGameEnded();

private:
	Engine();
	~Engine();

	Logger logger;
	GameStateManager gameStateManager;
	Input input;
	Window window;
	std::chrono::system_clock::time_point lastTick;
	static constexpr double Target_FPS = 30.0;
	double timer = 0;
	double aveFrameRate = 0;
	double frameCount = 0;
	
};