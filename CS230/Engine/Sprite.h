/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Sprite.h
Purpose: Header file for Sprite
Project: CS230
Author: Yujin Park
Creation date: 4/02/2020
-----------------------------------------------------------------*/

#pragma once

#include <string>
#include "BasicDataTypes.h"
#include "Texture.h"

class Sprite {
public:
	Sprite() {}
	void Load(std::string texturePath);
	void Draw(Vector2D position);
	void SetHotSpot(Vector2DInt position);
	Vector2DInt GetSize();
private:
	Texture texture;
	Vector2DInt hotSpot;
};
