/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Window.h
Purpose: Header file for Window
Project: CS230
Author: Kevin Wright / Yujin Park
Creation date: 3/19/2020
-----------------------------------------------------------------*/

#pragma once

#include "BasicDataTypes.h"

class Window {
public:
    void Init(std::string windowName);
    void Update();
	void Resize(int newWidth, int newHeight);
	Vector2DInt GetSize();
	void Clear(Color color);
	Vector2DInt screenSize = 0;

private:

};

void on_window_resized(int new_width, int new_height);