/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: BasicDataTypes.h
Purpose: Header file for BasicDataTypes
Project: CS230
Author: Yujin Park
Creation date: 4/02/2020
-----------------------------------------------------------------*/

#pragma once

struct Vector2D
{
public:
	constexpr Vector2D() noexcept = default;

	double x = 0;
	double y = 0;

	constexpr Vector2D operator+ (const Vector2D& vec2) const noexcept
	{
		Vector2D v;

		v.x = x + vec2.x;
		v.y = y + vec2.y;

		return v;
	}

	constexpr Vector2D operator- (const Vector2D& vec2) const noexcept
	{
		Vector2D v;

		v.x = x - vec2.x;
		v.y = y - vec2.y;

		return v;
	}
	constexpr Vector2D operator- (void) const noexcept
	{
		Vector2D v;
		v.x = -x;
		v.y = -y;

		return *this;
	}
	constexpr Vector2D operator* (const double value)const noexcept
	{
		Vector2D v;

		v.x = x * value;
		v.y = y * value;

		return v;

	}

	constexpr Vector2D operator/ (const double value)const noexcept
	{
		Vector2D v;

		v.x = x / value;
		v.y = y / value;

		return v;
	}

	constexpr Vector2D operator+=(const Vector2D& vec2)noexcept {
		this->x += vec2.x;
		this->y += vec2.y;

		return *this;
	}

	constexpr Vector2D operator-=(const Vector2D& vec2)noexcept {
		this->x -= vec2.x;
		this->y -= vec2.y;

		return *this;
	}


};

struct Vector2DInt
{
	constexpr Vector2DInt() {};
	constexpr Vector2DInt(int value) : Vector2DInt(value, value) {};
	constexpr Vector2DInt(int x_value, int y_value) : x(x_value), y(y_value) {};

	int x = 0;
	int y = 0;

	constexpr operator Vector2D() 
	{
		return Vector2D{static_cast<double>(x), static_cast<double>(y)};
	}

	constexpr Vector2DInt operator+= (Vector2DInt vec1) {
		return Vector2DInt{ (static_cast<int>(vec1.x += x)), (static_cast<int>(vec1.y += y)) };
	}
	constexpr Vector2DInt operator-= (Vector2DInt vec1) {
		return Vector2DInt{ (static_cast<int>(vec1.x -= x)), (static_cast<int>(vec1.y -= y)) };

	}
	constexpr Vector2DInt operator- () {
		return Vector2DInt{ (static_cast<int>(-x)), (static_cast<int>(-y)) };
	}

};
constexpr Vector2DInt operator- (Vector2DInt vec1, Vector2DInt vec2) {
	return Vector2DInt{ (static_cast<int>(vec1.x - vec2.x)), (static_cast<int>(vec1.y - vec2.y)) };
}

constexpr Vector2DInt operator + (Vector2DInt vec1, Vector2DInt vec2) {
	return Vector2DInt{ (static_cast<int>(vec1.x + vec2.x)), (static_cast<int>(vec1.y + vec2.y)) };
}

constexpr Vector2DInt operator* (Vector2DInt vec1, Vector2DInt vec2) {
	return Vector2DInt{ (static_cast<int>(vec1.x * vec2.x)), (static_cast<int>(vec1.y * vec2.y)) };
}

constexpr Vector2DInt operator/ (Vector2DInt vec1, Vector2DInt vec2) {
	return Vector2DInt{ (static_cast<int>(vec1.x / vec2.x)), (static_cast<int>(vec1.y /vec2.y)) };
}

typedef unsigned Color;

struct Rect {
	Vector2D bottomLeft;
	Vector2D topRight;
};