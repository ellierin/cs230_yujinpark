#include "Background.h"
#include "Texture.h"
#include "Engine.h"


void Background::Add(const std::string& texturePath, int level) {

	backgrounds.push_back({ Texture(texturePath), level });

	if (level == 1) {
		cameraRange.bottomLeft = { 0, 0 };
		cameraRange.topRight = backgrounds.back().texture.GetSize() - Engine::GetWindow().GetSize();
	}

}

void Background::Draw(Vector2D camera) {

	for (ParallaxInfo& levelInfo : backgrounds) {
		levelInfo.texture.Draw(Vector2D{ -camera.x / levelInfo.level, -camera.y });
	}

}

Rect Background::GetCameraRange()
{
	return cameraRange;
}

void Background::Unload() {
	backgrounds.clear();
}