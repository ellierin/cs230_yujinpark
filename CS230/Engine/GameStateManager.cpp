/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: GameStateManager.cpp
Purpose: Source file for Game State Manager
Project: CS230
Author: Kevin Wright / Yujin Park
Creation date: 3/19/2020
-----------------------------------------------------------------*/

#include "Engine.h"
#include "GameStateManager.h"


GameStateManager::GameStateManager() : currGameState(nullptr), nextGameState(nullptr), state(State::START) { }

void GameStateManager::AddGameState(GameState& gameState) {
	gameStates.push_back(&gameState);
}

void GameStateManager::Update(double dt) {
	
	if (Engine::GetGameStateManager().state == State::START) {
		Engine::GetGameStateManager().SetNextState(0);
		state = State::LOAD;
	}
	if (Engine::GetGameStateManager().state == State::LOAD) {
		currGameState = nextGameState;
		Engine::GetLogger().LogEvent(" Load " + currGameState->GetName());
		currGameState->Load();
		Engine::GetLogger().LogEvent(" Load Complete ");
		state = State::RUNNING;
	}
	if (Engine::GetGameStateManager().state == State::RUNNING) {
		if (currGameState == nextGameState) {
			currGameState->Update(dt);
			currGameState->Draw();
		}
		else if(currGameState != nextGameState) {
			state = State::UNLOAD;
		}
	}
	if (Engine::GetGameStateManager().state == State::UNLOAD) {
		currGameState->Unload();
		Engine::GetWindow().Clear(0x000000FF);

		Engine::GetLogger().LogEvent(" Unload " + currGameState->GetName());
		if (nextGameState == nullptr) {
			state = State::SHUTDOWN;
		}
		else if (nextGameState != nullptr) {
			state = State::LOAD;
		}
	}
	if (Engine::GetGameStateManager().state == State::SHUTDOWN) {
		state = State::EXIT;
	}
	if (Engine::GetGameStateManager().state == State::EXIT) {

	}
}

void GameStateManager::SetNextState(int initState) {
	nextGameState = gameStates[initState];
}

void GameStateManager::ReloadState() {
	state = State::UNLOAD;
}

void GameStateManager::Shutdown() {
	state = State::UNLOAD;
	nextGameState = nullptr;
}
