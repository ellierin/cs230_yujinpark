/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Ship.cpp
Purpose: Source file for ship
Project: CS230
Author: Yujin Park
Creation date: 4/03/2020
-----------------------------------------------------------------*/
#include "Ship.h"
#include "../Engine/Window.h"
#include "../Engine/Engine.h"

void Ship::Load()
{
	sprite.Load("assets/Ship.png");
	sprite.SetHotSpot(Vector2DInt{ 50, 41 });
	position = initPosition;
	velocity = { 0,0 };
}

void Ship::Update(double dt)
{
	const double xDrag = velocity.x / 100;
	const double yDrag = velocity.y / 100;

	if (moveLeftKey.IsKeyDown() == true)
	{
		velocity.x -= acceleration * dt;

		if (velocity.x < -Ship::maxVelocity) {
			velocity.x = -Ship::maxVelocity;
		}

		if (velocity.x > 0 && moveLeftKey.IsKeyReleased() == true) {
			velocity.x += velocity.x * xDrag * dt; 
		}

	}


	if (moveRightKey.IsKeyDown() == true)
	{
		velocity.x += acceleration * dt;

		if (velocity.x > Ship::maxVelocity) {
			velocity.x = Ship::maxVelocity;
		}
		if (velocity.x < 0 && moveRightKey.IsKeyReleased() == true ) {
			velocity.x -= velocity.x * xDrag * dt;
		}

	}

	if (moveLeftKey.IsKeyDown() == false && moveRightKey.IsKeyDown() == false) {
	
		if (velocity.x < xDrag && velocity.x > -xDrag)
		{
			velocity.x = 0;
		}
		if (velocity.x >= xDrag)
		{
			velocity.x -= velocity.x * xDrag * dt;
		}
		if (velocity.x <= -xDrag)
		{
			velocity.x += velocity.x * xDrag * dt;
		}

	}

	if (moveUpKey.IsKeyDown() == true)
	{
		velocity.y += acceleration * dt;

		if (velocity.y > Ship::maxVelocity) {
			velocity.y = Ship::maxVelocity;
		}
		if (velocity.y > 0 && moveUpKey.IsKeyReleased() == true) {
			velocity.y -= velocity.y * yDrag * dt;
		}

	}
	if (moveDownKey.IsKeyDown() == true)
	{
		velocity.y -= acceleration * dt;

		if (velocity.y < -Ship::maxVelocity) {
			velocity.y = -Ship::maxVelocity;
		}
		if (velocity.y < 0 && moveDownKey.IsKeyReleased() == true) {
			velocity.y += velocity.y * yDrag * dt;
		}
	}


	if (moveDownKey.IsKeyDown() == false && moveUpKey.IsKeyDown() == false) {

		if (velocity.y < yDrag && velocity.y > -yDrag)
		{
			velocity.y = 0;
		}
		if (velocity.y >= yDrag)
		{
			velocity.y -= velocity.y * yDrag * dt;
		}
		if (velocity.y <= -yDrag)
		{
			velocity.y += velocity.y * yDrag * dt;
		}

	}

	position += velocity * dt;

	TestForWrap();
}



void Ship::Draw()
{
	sprite.Draw(position);
}


void Ship::TestForWrap() {

	if (position.x <= 0) {
		position.x = Engine::GetWindow().GetSize().x;
	}

	else if (position.x >= Engine::GetWindow().GetSize().x) {
		position.x = 0;
	}

	if (position.y <= 0) {
		position.y = Engine::GetWindow().GetSize().y;
	}

	else if (position.y >= Engine::GetWindow().GetSize().y) {
		position.y = 0;
	}

}