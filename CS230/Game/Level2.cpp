/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level2.cpp
Purpose: source file for Level 2
Project: CS230
Author: Yujin Park
Creation date: 3/25/2020
-----------------------------------------------------------------*/

#include "Screens.h"
#include "Splash.h"
#include "Level2.h"

void Level2::Load()
{
	ship.Load();
}

void Level2::Update(double dt)
{
	if (levelNext.IsKeyReleased()) {
		Engine::GetGameStateManager().Shutdown();
	}

	if (levelReload.IsKeyReleased()) {
		Engine::GetGameStateManager().ReloadState();
	}
	ship.Update(dt);
}

void Level2::Unload(){

}

void Level2::Draw() {
	Engine::GetWindow().Clear(0x000000FF);
	background.Draw({ 0, 0 });
	ship.Draw();
}