/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Hero.h
Purpose: header file for hero
Project: CS230
Author: Yujin Park
Creation date: 4/03/2020
-----------------------------------------------------------------*/

#pragma once

#include "../Engine/BasicDataTypes.h"
#include "../Engine/Sprite.h"
#include "../Engine/Input.h"

class Hero {
public:
	Hero(Vector2D startPos, Vector2D& cameraPos) : initPosition(startPos),
		cameraPos(cameraPos), velocity({ 0,0 }), jumping(false), jumpRelease(false),
		moveLeftKey(Input::KeyboardButton::Left), moveRightKey(Input::KeyboardButton::Right),
		JumpKey(Input::KeyboardButton::Up) {}
	void Load();
	void Update(double dt, Vector2D&);
	void Draw(Vector2D h_position);
	Vector2D GetPosition() { return position; };
	
private:
	Sprite sprite;
	Vector2D initPosition;
	Vector2D position;
	Vector2D velocity;
	Vector2D cameraPos;

	Input::InputKey moveLeftKey;
	Input::InputKey moveRightKey;
	Input::InputKey JumpKey;

	static const double jumpVelocity;
	static const double xAccel;
	static const double xDrag;
	static const double maxXVelocity;

	bool jumping = false;
	bool jumpRelease = false;

};


