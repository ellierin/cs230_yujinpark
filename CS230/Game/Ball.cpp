#include "Ball.h"
#include "Level1.h"


const double Ball::bounceVelocity = 300.0;

void Ball::Load()
{
	sprite.Load("assets/Ball.png");
	sprite.SetHotSpot(Vector2DInt{ sprite.GetSize().x / 2 , 0 });
	position = initPosition;
}

void Ball::Update(double dt)
{
		velocity.y -= Level1::gravity * dt;
	
	if (position.y == Level1::floor) {
		velocity.y = Ball::bounceVelocity;
	}
	position += velocity * dt;

	if (position.y < Level1::floor) {
		velocity.y = 0;
		position.y = Level1::floor;
	}

	
}

void Ball::Unload()
{
}

void Ball::Draw(Vector2D camerPos)
{
	sprite.Draw(camerPos);
}
