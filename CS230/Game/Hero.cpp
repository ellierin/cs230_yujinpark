/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Hero.cpp
Purpose: Source file for hero
Project: CS230
Author: Yujin Park
Creation date: 4/03/2020
-----------------------------------------------------------------*/
#include "Hero.h"
#include "Level1.h"
#include "../Engine/Engine.h"
#include "..//Engine/Background.h"

const double Hero::maxXVelocity = 380.0;
const double Hero::xAccel = 300.0;
const double Hero::xDrag = 230.0;
const double Hero::jumpVelocity = 400.0;

void Hero::Load()
{
	sprite.Load("assets/Hero.png");
	sprite.SetHotSpot(Vector2DInt{ 56, 14 });
	position = initPosition;
	velocity = { 0,0 };
	jumping = false;
	jumpRelease = false;
}

void Hero::Update(double dt, Vector2D& camPos)
{
	
		if (moveLeftKey.IsKeyDown() == true)
		{

			Engine::GetLogger().LogDebug("X velocity = " + std::to_string(velocity.x));

			velocity.x -= xAccel * dt;

			if (velocity.x < -Hero::maxXVelocity) {
				velocity.x = -Hero::maxXVelocity;
			}

			if ( velocity.x > 0) {
				Engine::GetLogger().LogDebug("X velocity = " + std::to_string(velocity.x));
				velocity.x += xDrag * dt;
			}
			
		}

		if (moveLeftKey.IsKeyDown() == false && moveRightKey.IsKeyDown() == false) {
			Engine::GetLogger().LogDebug("X velocity = " + std::to_string(velocity.x));
			if (velocity.x <xDrag && velocity.x >-xDrag)
			{
				velocity.x = 0;
			}
			if (velocity.x >= xDrag)
			{
				velocity.x -= xDrag * dt;
			}
			if (velocity.x <= -xDrag)
			{
				velocity.x += xDrag * dt;
			}

		}



		if (moveRightKey.IsKeyDown() == true)
		{
			Engine::GetLogger().LogDebug("X velocity = " + std::to_string(velocity.x));

			velocity.x += xAccel * dt;

			if (velocity.x > Hero::maxXVelocity) {
				velocity.x = Hero::maxXVelocity;
			}

			if (moveRightKey.IsKeyReleased() == true &&  velocity.x < 0  ) {
				Engine::GetLogger().LogDebug("X velocity = " + std::to_string(velocity.x));
				velocity.x -= xDrag * dt;

			}
		}


	if (jumping == true) {

		if (jumpRelease == false && JumpKey.IsKeyDown()) {
			Engine::GetLogger().LogEvent("Jump Y Position = " + std::to_string(position.y));
			jumpRelease = true;
		}

		if (jumpRelease == false && JumpKey.IsKeyReleased()) {			
			velocity.y = 0;
			jumpRelease = true;	
		
		}

		velocity.y -= Level1::gravity * dt;

		if (jumpRelease == false && velocity.y <= 0) {
			jumpRelease = true;
		}
	}

	if (JumpKey.IsKeyDown() && position.y == Level1::floor) {
		jumping = true;
		jumpRelease = false;
		velocity.y = Hero::jumpVelocity;
	}

	position += velocity * dt;

	if (jumping == true && position.y < Level1::floor) {
		velocity.y = 0;
		position.y = Level1::floor;
		jumping = false;
		Engine::GetLogger().LogEvent("Land Y Position = " + std::to_string(position.y));
	}

	if (velocity.y != 0) {
		Engine::GetLogger().LogDebug("Y Position = " + std::to_string(position.y));
	}

	
	if (position.x > camPos.x + Engine::GetWindow().GetSize().x) {
		position.x = camPos.x + Engine::GetWindow().GetSize().x - sprite.GetSize().x;
	}

	if (position.x < camPos.x) {
		position.x = camPos.x + sprite.GetSize().x;
		velocity.x = 0;
	}


}

void Hero::Draw(Vector2D h_position)
{
	sprite.Draw(h_position);
}
