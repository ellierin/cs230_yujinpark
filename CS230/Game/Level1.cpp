/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level1.cpp
Purpose: source file for Level 1
Project: CS230
Author: Yujin Park
Creation date: 3/25/2020
-----------------------------------------------------------------*/

#include "Screens.h"
#include "Splash.h"
#include "Level1.h"
#include "../engine/Engine.h"


const double Level1::floor = 115;
const double Level1::gravity = 800;
const double Level1::cameraPlayerMoveRange = .35;

void Level1::Load()
{
	ball1.Load();
	ball2.Load();
	ball3.Load();

	hero.Load();
	Engine::GetLogger().LogEvent("Load Level1");
	background.Add("assets/clouds.png", 4);
	background.Add("assets/Mountains.png", 2);
	background.Add("assets/foreground.png" , 1);
}

void Level1::Update(double dt)
{

	double cameraDistance = Engine::GetWindow().GetSize().x * Level1::cameraPlayerMoveRange;

	if (hero.GetPosition().x - cameraDistance <= background.GetCameraRange().topRight.x) {
		if (hero.GetPosition().x - cameraPos.x > cameraDistance) {

			cameraPos.x = hero.GetPosition().x - cameraDistance;
		}
	}

	hero.Update(dt,cameraPos);

	ball1.Update(dt);
	ball2.Update(dt);
	ball3.Update(dt);

	if (levelNext.IsKeyReleased()){
		Engine::GetGameStateManager().SetNextState(LEVEL2);
	}
	if (levelReload.IsKeyReleased()) {
		Engine::GetGameStateManager().ReloadState();
	}

	if (cameraPos.x < background.GetCameraRange().bottomLeft.x) {
		cameraPos.x = background.GetCameraRange().bottomLeft.x;
	
	}
	if (cameraPos.x > background.GetCameraRange().topRight.x) {
		cameraPos.x = background.GetCameraRange().topRight.x;
		
	}
	
	if (cameraPos.y < background.GetCameraRange().bottomLeft.y) {
		cameraPos.y = background.GetCameraRange().bottomLeft.y;
	}
	if (cameraPos.y > background.GetCameraRange().topRight.y) {
		cameraPos.y = background.GetCameraRange().topRight.y;
	}

}

void Level1::Unload() 
{
}

void Level1::Draw() {
	Engine::GetWindow().Clear(0x3399DAFF);
	background.Draw(cameraPos);

	ball1.Draw(ball1.GetPosition() - cameraPos);

	ball2.Draw(ball2.GetPosition() - cameraPos);

	ball3.Draw(ball3.GetPosition() - cameraPos);

	hero.Draw(hero.GetPosition() - cameraPos);


}