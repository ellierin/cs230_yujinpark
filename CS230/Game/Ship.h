/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Ship.h
Purpose: Header file for Ship
Project: CS230
Author: Yujin Park
Creation date: 4/03/2020
-----------------------------------------------------------------*/
#pragma once

#include "../Engine/Input.h"
#include "../Engine/Sprite.h"
#include "../Engine/Window.h"

class Ship {
public:
	Ship(Vector2D startPos) : initPosition(startPos), 
		moveLeftKey(Input::KeyboardButton::A), moveRightKey(Input::KeyboardButton::D),
		moveUpKey(Input::KeyboardButton::W), moveDownKey(Input::KeyboardButton::S){}
	void Load();
	void Update(double dt);
	void Draw();
	

private:
	Sprite sprite;
	Vector2D initPosition;
	Vector2D position;
	Vector2D velocity;

	Input::InputKey moveLeftKey;
	Input::InputKey moveRightKey;
	Input::InputKey moveUpKey;
	Input::InputKey moveDownKey;

	const double acceleration = 130.0;
	const int maxVelocity = 300;
	void TestForWrap();
	
};
