/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level1.h
Purpose: Header file for Level 1
Project: CS230
Author: Yujin Park
Creation date: 3/26/2020
-----------------------------------------------------------------*/

#pragma once

#include "../Engine/Engine.h"
#include "Hero.h"
#include"../Engine/BasicDataTypes.h"
#include "../Engine/Background.h"
#include "Ball.h"

class Level1 : public GameState {
public:
	static const double floor;
	Level1() : hero({ 100, Level1::floor }, cameraPos), ball1({ 600, Level1::floor }), ball2({2700, Level1::floor}), ball3({ 4800, Level1::floor }),
		levelNext(Input::KeyboardButton::Enter), levelReload(Input::KeyboardButton::R) {}

	void Load() override;
	void Update(double dt) override;
	void Unload() override;

	std::string GetName() override { return "Level 1 "; }
	void Draw() override;
	static const double gravity;
	static const double cameraPlayerMoveRange;
	
private:
	Hero hero;

	Ball ball1;
	Ball ball2;
	Ball ball3;

	Background background;
	Vector2D cameraPos;
	Vector2D initPosition;
	Input::InputKey levelNext;
	Input::InputKey levelReload;
};