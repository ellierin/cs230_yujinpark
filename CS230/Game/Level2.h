/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level2.h
Purpose: Header file for Level 2
Project: CS230
Author: Yujin Park
Creation date: 3/26/2020
-----------------------------------------------------------------*/

#pragma once

#include "../Engine/Engine.h"
#include "../Engine/GameState.h"
#include "ship.h"

class Level2 : public GameState {
public:
	Level2() : ship({ static_cast<double>(Engine::GetWindow().GetSize().x / 2), static_cast<double>(Engine::GetWindow().GetSize().y / 2) }),
		levelNext(Input::KeyboardButton::Enter),levelReload(Input::KeyboardButton::R) {}

	void Load() override;
	void Update(double) override;
	void Unload() override;
	std::string GetName() override { return "Level 2"; }

	void Draw() override;

private:
	Ship ship;
	Texture background;
	Input::InputKey levelReload;
	Input::InputKey levelNext;
};