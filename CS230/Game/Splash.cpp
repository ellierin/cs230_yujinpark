/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Splash.cpp
Purpose: source file for Splash.h
Project: CS230
Author: Yujin Park
Creation date: 3/26/2020
-----------------------------------------------------------------*/
#include "Screens.h"
#include "Splash.h"
#include "../Engine/Texture.h"
#include <doodle\drawing.hpp>

void Splash::Load() {
	std::string texturePath = "assets/DigiPen_BLACK_1024px.png";
	Logo.Load(texturePath);
}

void Splash::Update(double dt) {
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	Timer += dt;
	if (Timer >= 3) {
		Engine::GetGameStateManager().SetNextState(LEVEL1);
	}
}

void Splash::Unload() {
	Timer = 0;
}

void Splash::Draw() {
	Engine::GetWindow().Clear(0xFFFFFFFF);
	Logo.Draw(Engine::GetWindow().GetSize() / 2 - Logo.GetSize() / 2);
}

