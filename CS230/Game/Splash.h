/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Splash.h
Purpose: Header file for Splash
Project: CS230
Author: Kevin Wright / Yujin Park
Creation date: 3/19/2020
-----------------------------------------------------------------*/

#pragma once

#include "../Engine/Engine.h"
#include "../Engine/GameState.h"
#include "../Engine/Texture.h"

class Splash : public GameState {
public:
	void Load() override;
	void Update(double dt) override;
	void Unload() override;

	std::string GetName() override { return "Splash"; }
	void Draw() override;

private:
	Texture Logo;
	double Timer;
};

 