#pragma once
#include "../Engine/BasicDataTypes.h"
#include "../Engine/Sprite.h"


class Ball {
public:
	Ball(Vector2D b_startPos) : initPosition(b_startPos) {}

	void Load();
	void Update(double dt);
	void Unload();
	void Draw(Vector2D cameraPos);
	Vector2D GetPosition() { return position; };

private:
	Sprite sprite;
	Vector2D initPosition;
	Vector2D velocity;
	Vector2D position;

	static const double bounceVelocity;
};